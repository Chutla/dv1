package com.example.lc4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        generate()
    }

    private fun generate() {
        val but = findViewById<Button>(R.id.Generator)
        val randomnumb = findViewById<TextView>(R.id.Number)
        val numbershow = findViewById<TextView>(R.id.numbershower)

        but.setOnClickListener(){
            val rnds:Int = (-100..100).random()
            numbershow.text = "" + rnds
            if(rnds%5==0) {
                randomnumb.text = "Yes!"
            }
            else {
                randomnumb.text = "No!"
            }
            }

        }
    }
